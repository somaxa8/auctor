package lexer

data class Token(
    val type: TokenType,
    val text: String
)
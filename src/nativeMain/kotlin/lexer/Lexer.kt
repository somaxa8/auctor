package lexer

class Lexer(
    private val input: String,
) {
    private var position = 0

    private val keywords = mapOf(
        "entity" to TokenType.ENTITY,
        "relationship" to TokenType.RELATIONSHIP,
        "enum" to TokenType.ENUM,
        "to" to TokenType.TO,
        "String" to TokenType.STRING,
        "Integer" to TokenType.INTEGER,
        "BigDecimal" to TokenType.BIGDECIMAL,
        "Instant" to TokenType.INSTANT,
        "required" to TokenType.REQUIRED
    )

    fun tokenize(): List<Token> {
        val tokens = mutableListOf<Token>()

        while (position < input.length) {
            when (val currentChar = input[position]) {
                '{' -> {
                    tokens.add(Token(TokenType.LBRACE, currentChar.toString()))
                    position++
                }
                '}' -> {
                    tokens.add(Token(TokenType.RBRACE, currentChar.toString()))
                    position++
                }
                ',' -> {
                    tokens.add(Token(TokenType.COMMA, currentChar.toString()))
                    position++
                }
                ' ', '\t', '\r', '\n' -> position++
                else -> {
                    if (currentChar.isLetter()) {
                        tokens.add(lexIdentifierOrKeyword())
                    } else {
                        throw IllegalArgumentException("Unexpected character: $currentChar")
                    }
                }
            }
        }
        tokens.add(Token(TokenType.EOF, ""))
        return tokens
    }

    private fun lexIdentifierOrKeyword(): Token {
        val start = position
        while (position < input.length && (input[position].isLetterOrDigit() || input[position] == '_')) {
            position++
        }
        val text = input.substring(start, position)
        val type = keywords[text] ?: TokenType.IDENTIFIER
        return Token(type, text)
    }
}
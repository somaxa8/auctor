package generator

import parser.*
import parser.Validation.REQUIRED
import utils.NameVariants
import utils.toPascalCase
import utils.toPlural

class CodeGenerator(private val document: JDLDocument) {
    fun generate(template: String, types: Map<String, String>) {
         val entities = document.entities.map { entity ->
            val processedTemplate = processEntity(template, entity, types)

            val relationshipsByEntity = document.relationships.find { relationship ->
                relationship.relationships.any { pair ->
                    pair.first.startsWith(entity.name)
                }
            }

             processRelationships(processedTemplate, relationshipsByEntity)
        }

        entities.forEach {
            println(it)
        }
    }

    private fun processRelationships(template: String, relationship: Relationship?): String {
        val relationshipBlockRegex = Regex("\\{\\{#relationships}}([\\s\\S]*?)\\{\\{/relationships}}")
        val relationshipBlockMatch = relationshipBlockRegex.find(template)

        var result = template

        if (relationship == null) {
            result = result.replace(relationshipBlockRegex, "")
            return result
        }

        if (relationshipBlockMatch != null) {
            val relationshipBlockTemplate = relationshipBlockMatch.groupValues[1].trim()

            val relationshipSection = relationship.relationships.joinToString("\n    ") { relation ->
                injectRelationshipInfo(relation, relationship.type, relationshipBlockTemplate) + "\n"
            }

            result = result.replace(relationshipBlockRegex, relationshipSection)
        }

        return result
    }

    private fun injectRelationshipInfo(relationship: Pair<String, String>, relationshipType: RelationshipType, relationshipBlockTemplate: String): String {
        val secondNameVariants = NameVariants(relationship.second)

        return relationshipBlockTemplate
            .replace("{{relationshipName}}", secondNameVariants.pascalCaseName)
            .replace("{{relationshipCamelCaseName}}", secondNameVariants.camelCaseName)
            .replace("{{relationshipSnakeCaseName}}", secondNameVariants.snakeCaseName)
            .replace("{{relationshipUpperSnakeCaseName}}", secondNameVariants.upperSnakeCaseName)
            .replace("{{relationshipKebabCaseName}}", secondNameVariants.kebabCaseName)
            .replace("{{relationshipTypeName}}", relationshipType.name.toPascalCase())

    }

    private fun processEntity(template: String, entity: Entity, types: Map<String, String>): String {
        var result = injectEntityInfo(template, entity)

        val attributeBlockRegex = Regex("\\{\\{#attributes}}([\\s\\S]*?)\\{\\{/attributes}}")
        val attributeBlockMatch = attributeBlockRegex.find(result)

        if (entity.attributes.isEmpty()) {
            result = result.replace(attributeBlockRegex, "")
            return result
        }

        if (attributeBlockMatch != null) {
            val attributeBlockTemplate = attributeBlockMatch.groupValues[1].trim()

            val attributesSection = entity.attributes.joinToString("\n    ") { attribute ->
                injectAttributeInfo(attribute, types, attributeBlockTemplate) + "\n"
            }

            result = result.replace(attributeBlockRegex, attributesSection)
        }

        return result
    }

    private fun injectAttributeInfo(attribute: Attribute, types: Map<String, String>, attributeBlockTemplate: String): String {
        val attributeNameVariants = getNameVariants(attribute.name)
        val pluralAttributeNameVariants = getNameVariants(attribute.name.toPlural())

        val nullable = !attribute.validations.contains(REQUIRED)

        val type = types[attribute.type.toString()]
            ?: throw Exception("Types does not exist")

        return attributeBlockTemplate
            .replace("{{type}}", type)
            .replace("{{nullable}}", nullable.toString())
            // Normal names
            .replace("{{fieldName}}", attributeNameVariants.camelCaseName)
            .replace("{{fieldNameCamelCase}}", attributeNameVariants.camelCaseName)
            .replace("{{fieldNamePascalCase}}", attributeNameVariants.pascalCaseName)
            .replace("{{fieldNameSnakeCase}}", attributeNameVariants.snakeCaseName)
            .replace("{{fieldNameUpperSnakeCase}}", attributeNameVariants.upperSnakeCaseName)
            .replace("{{fieldNameKebabCase}}", attributeNameVariants.kebabCaseName)
            // Plural names
            .replace("{{pluralFieldName}}", pluralAttributeNameVariants.camelCaseName)
            .replace("{{pluralFieldNameCamelCase}}", pluralAttributeNameVariants.camelCaseName)
            .replace("{{pluralFieldNamePascalCase}}", pluralAttributeNameVariants.pascalCaseName)
            .replace("{{pluralFieldNameSnakeCase}}", pluralAttributeNameVariants.snakeCaseName)
            .replace("{{pluralFieldNameUpperSnakeCase}}", pluralAttributeNameVariants.upperSnakeCaseName)
            .replace("{{pluralFieldNameKebabCase}}", pluralAttributeNameVariants.kebabCaseName)

    }

    private fun injectEntityInfo(template: String, entity: Entity): String {
        val entityNameVariants = getNameVariants(entity.name)
        val pluralEntityNameVariants = getNameVariants(entity.name.toPlural())

        return template
            // Normal Names
            .replace("{{entityName}}", entityNameVariants.pascalCaseName)
            .replace("{{entityCamelCaseName}}", entityNameVariants.camelCaseName)
            .replace("{{entityUpperSnakeCaseName}}", entityNameVariants.upperSnakeCaseName)
            .replace("{{entitySnakeCaseName}}", entityNameVariants.snakeCaseName)
            .replace("{{entityKebabCaseName}}", entityNameVariants.kebabCaseName)
            // Plural names
            .replace("{{pluralEntityName}}", pluralEntityNameVariants.pascalCaseName)
            .replace("{{pluralEntityCamelCaseName}}", pluralEntityNameVariants.camelCaseName)
            .replace("{{pluralEntityUpperSnakeCaseName}}", pluralEntityNameVariants.upperSnakeCaseName)
            .replace("{{pluralEntitySnakeCaseName}}", pluralEntityNameVariants.snakeCaseName)
            .replace("{{pluralEntityKebabCaseName}}", pluralEntityNameVariants.kebabCaseName)
    }

    private fun getNameVariants(name: String): NameVariants {
        return NameVariants(name)
    }

}

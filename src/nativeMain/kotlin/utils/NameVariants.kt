package utils

class NameVariants (
    private val name: String
) {
    val camelCaseName: String
        get() = name.toCamelCase()

    val pascalCaseName: String
        get() = name.toPascalCase()

    val snakeCaseName: String
        get() = name.toSnakeCase()

    val upperSnakeCaseName: String
        get() = name.toUpperSnakeCase()

    val kebabCaseName: String
        get() = name.toKebabCase()
}

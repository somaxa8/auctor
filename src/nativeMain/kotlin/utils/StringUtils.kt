package utils

fun String.toCamelCase(): String {
    return this.lowercase().split(" ", "_", "-")
        .mapIndexed { index, word ->
            if (index == 0) word.lowercase()
            else word.replaceFirstChar { it.uppercase() }
        }
        .joinToString("")
}

fun String.toPascalCase(): String {
    return this.lowercase().toCamelCase().replaceFirstChar { it.uppercase() }
}

fun String.toSnakeCase(): String {
    return this.lowercase().split(" ", "_", "-").joinToString("_") { it.lowercase() }
}

fun String.toUpperSnakeCase(): String {
    return this.lowercase().toSnakeCase().replaceFirstChar { it.uppercase() }
}

fun String.toKebabCase(): String {
    return this.split(" ", "_", "-").joinToString("-") { it.lowercase() }
}

fun String.toPlural(): String {
    return when {
        this.endsWith("y", ignoreCase = true) && this.length > 1 && !this[this.length - 2].isVowel() -> this.dropLast(1) + "ies"
        this.endsWith("ch", ignoreCase = true) || this.endsWith("sh", ignoreCase = true) || this.endsWith("s", ignoreCase = true) || this.endsWith("x", ignoreCase = true) || this.endsWith("z", ignoreCase = true) -> this + "es"
        else -> this + "s"
    }
}

fun Char.isVowel(): Boolean {
    return this.lowercaseChar() in listOf('a', 'e', 'i', 'o', 'u')
}

fun String.toKebabCasePlural(): String {
    return this.toKebabCase().toPlural()
}

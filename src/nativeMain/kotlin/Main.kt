import generator.CodeGenerator
import io.File
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.required
import kotlinx.serialization.json.Json
import lexer.Lexer
import parser.Parser

fun main(args: Array<String>) {
    val parser = ArgParser("auctor")
    val jdl by parser.option(ArgType.String, shortName = "jdl", description = "JDL file path").required()
    val generate by parser.option(ArgType.String, shortName = "g", description = "Generate code")

    parser.parse(args)

    val jdlFileContent = File.readFile(jdl)
    val lexer = Lexer(jdlFileContent)
    val tokens = lexer.tokenize()
    val jdlDocument = Parser(tokens).parse()

    val template = templateLoader()
    val types = typesLoader()
    CodeGenerator(jdlDocument).generate(template, types)
}

fun templateLoader(): String {
    return File.readFile("./.auctor/entity.kt.auctor")
}

fun typesLoader(): Map<String, String> {
    val typesJson = File.readFile("./.auctor/types.json")

    return Json.decodeFromString<Map<String, String>>(typesJson)
}

package parser

import lexer.Token
import lexer.TokenType

class Parser(private val tokens: List<Token>) {
    private var position = 0

    fun parse(): JDLDocument {
        val entities = mutableListOf<Entity>()
        val relationships = mutableListOf<Relationship>()
        val enums = mutableListOf<EnumType>()

        while (currentToken().type != TokenType.EOF) {
            when (currentToken().type) {
                TokenType.ENTITY -> entities.add(parseEntity())
                TokenType.RELATIONSHIP -> relationships.add(parseRelationship())
                TokenType.ENUM -> enums.add(parseEnum())
                else -> throw IllegalArgumentException("Unexpected token: ${currentToken()}")
            }
        }
        return JDLDocument(entities, relationships, enums)
    }

    private fun parseEntity(): Entity {
        consume(TokenType.ENTITY)
        val name = consume(TokenType.IDENTIFIER).text
        consume(TokenType.LBRACE)
        val attributes = mutableListOf<Attribute>()
        while (currentToken().type != TokenType.RBRACE) {
            attributes.add(parseAttribute())
        }
        consume(TokenType.RBRACE)
        return Entity(name, attributes)
    }

    private fun parseAttribute(): Attribute {
        val name = consume(TokenType.IDENTIFIER).text
        val type = consumeType()
        val validations = mutableListOf<Validation>()
        while (currentToken().type == TokenType.REQUIRED) {
            validations.add(Validation.REQUIRED)
            position++
        }
        return Attribute(name, type, validations)
    }

    private fun consumeType(): AttributeType {
        return when (currentToken().type) {
            TokenType.STRING -> AttributeType.STRING
            TokenType.INTEGER -> AttributeType.INTEGER
            TokenType.BIGDECIMAL -> AttributeType.BIGDECIMAL
            TokenType.INSTANT -> AttributeType.INSTANT
            else -> throw IllegalArgumentException("Unexpected token type: ${currentToken().type}")
        }.also { position++ }
    }

    private fun parseRelationship(): Relationship {
        consume(TokenType.RELATIONSHIP)
        val type = consumeRelationshipType()
        consume(TokenType.LBRACE)
        val relationships = mutableListOf<Pair<String, String>>()
        while (currentToken().type != TokenType.RBRACE) {
            val from = consume(TokenType.IDENTIFIER).text
            consume(TokenType.LBRACE)
            val to = consume(TokenType.IDENTIFIER).text
            consume(TokenType.RBRACE)
            consume(TokenType.TO)
            val destination = consume(TokenType.IDENTIFIER).text
            relationships.add(from to destination)
        }
        consume(TokenType.RBRACE)
        return Relationship(type, relationships)
    }

    private fun consumeRelationshipType(): RelationshipType {
        return when (currentToken().type) {
            TokenType.IDENTIFIER -> {
                when (currentToken().text) {
                    "OneToOne" -> RelationshipType.ONE_TO_ONE
                    "OneToMany" -> RelationshipType.ONE_TO_MANY
                    "ManyToOne" -> RelationshipType.MANY_TO_ONE
                    "ManyToMany" -> RelationshipType.MANY_TO_MANY
                    else -> throw IllegalArgumentException("Unexpected relationship type: ${currentToken().text}")
                }
            }
            else -> throw IllegalArgumentException("Unexpected token type: ${currentToken().type}")
        }.also { position++ }
    }

    private fun parseEnum(): EnumType {
        consume(TokenType.ENUM)
        val name = consume(TokenType.IDENTIFIER).text
        consume(TokenType.LBRACE)
        val values = mutableListOf<String>()
        while (currentToken().type != TokenType.RBRACE) {
            values.add(consume(TokenType.IDENTIFIER).text)
            if (currentToken().type == TokenType.COMMA) {
                consume(TokenType.COMMA)
            }
        }
        consume(TokenType.RBRACE)
        return EnumType(name, values)
    }

    private fun consume(expectedType: TokenType): Token {
        val token = currentToken()
        if (token.type != expectedType) {
            throw IllegalArgumentException("Expected token type $expectedType but found ${token.type}")
        }
        position++
        return token
    }

    private fun currentToken(): Token {
        return tokens[position]
    }
}

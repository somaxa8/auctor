package parser

data class JDLDocument(
    val entities: List<Entity>,
    val relationships: List<Relationship>,
    val enums: List<EnumType>
)

data class Entity(
    val name: String,
    val attributes: List<Attribute>,
)

data class Attribute(
    val name: String,
    val type: AttributeType,
    val validations: List<Validation>,
)

enum class AttributeType {
    STRING, INTEGER, BIGDECIMAL, INSTANT
}

enum class Validation {
    REQUIRED
}

data class Relationship(
    val type: RelationshipType,
    val relationships: List<Pair<String, String>>
)

enum class RelationshipType {
    ONE_TO_ONE, ONE_TO_MANY, MANY_TO_ONE, MANY_TO_MANY
}

data class EnumType(
    val name: String,
    val values: List<String>
)

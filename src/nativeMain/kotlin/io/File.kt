package io

import kotlinx.cinterop.*
import platform.posix.*

class File {

    companion object {

        @OptIn(ExperimentalForeignApi::class)
        fun readFile(fileName: String): String {
            val file = fopen(fileName, "r") ?: throw Error("Cannot open file $fileName")
            val buffer = StringBuilder()
            try {
                memScoped {
                    val lineBuffer = allocArray<ByteVar>(1024)
                    while (fgets(lineBuffer, 1024, file) != null) {
                        buffer.append(lineBuffer.toKString())
                    }
                }
            } finally {
                fclose(file)
            }
            return buffer.toString()
        }
    }

}
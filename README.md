# Auctor

Auctor is a tool for generating code from a domain-specific language (DSL) inspired by JDL (JHipster Domain Language). This tool is written in Kotlin Native and does not depend on any Java libraries.

## Features

- **Lexer**: Converts the input text into a list of tokens.
- **Parser**: Converts the list of tokens into an abstract syntax tree (AST).
- **Code Generator**: Processes the AST and generates the corresponding code.

## Requirements

- [Kotlin Native](https://kotlinlang.org/docs/native-overview.html)

## Installation

1. Clone the repository:
    ```sh
    git clone https://github.com/your-username/auctor.git
    cd auctor
    ```

2. Set up the project with Gradle:
    ```sh
    ./gradlew build
    ```

## Future Proposal

The future vision for Auctor is to support extensive customization by allowing users to define their own templates and type definitions for various programming languages and frameworks. This will be achieved by using a dedicated folder in the system called `.auctor`, where users can add subfolders for the languages or frameworks they plan to use. Inside these subfolders, users can include template files and a type definition file (`types.json`). This will make Auctor a versatile tool capable of generating code for any language or framework, thus making it highly customizable.

### Example: Generating Code for a Spring Boot Application

To illustrate this proposal, let's assume you want to generate code for a Spring Boot application. Here's how you can set it up:

1. **Create a Directory for Spring Boot Templates**

   First, create a directory structure under `.auctor` for Spring Boot:
    ```
    .auctor/
    └── spring/
        ├── entity.java.auctor
        ├── repository.java.auctor
        ├── service.java.auctor
        ├── controller.java.auctor
        └── types.json
    ```

2. **Define Template Files**

   Each template file (`*.java.auctor`) will contain the template for generating the corresponding code. For example:

    - **entity.java.auctor**:
        ```java
        package {{packageName}};

        import javax.persistence.*;

        @Entity
        public class {{entityName}} {

            @Id
            @GeneratedValue(strategy = GenerationType.IDENTITY)
            private Long id;

            {{#attributes}}
            private {{type}} {{name}};
            {{/attributes}}

            // Getters and setters
        }
        ```

    - **repository.java.auctor**:
        ```java
        package {{packageName}};

        import org.springframework.data.jpa.repository.JpaRepository;

        public interface {{entityName}}Repository extends JpaRepository<{{entityName}}, Long> {
        }
        ```

    - **service.java.auctor**:
        ```java
        package {{packageName}};

        import java.util.List;

        public interface {{entityName}}Service {
            List<{{entityName}}> findAll();
            {{entityName}} findById(Long id);
            {{entityName}} save({{entityName}} entity);
            void deleteById(Long id);
        }
        ```

    - **controller.java.auctor**:
        ```java
        package {{packageName}};

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.*;

        import java.util.List;

        @RestController
        @RequestMapping("/api/{{entityNameLower}}")
        public class {{entityName}}Controller {

            @Autowired
            private {{entityName}}Service {{entityNameLower}}Service;

            @GetMapping
            public List<{{entityName}}> getAll() {
                return {{entityNameLower}}Service.findAll();
            }

            @GetMapping("/{id}")
            public {{entityName}} getById(@PathVariable Long id) {
                return {{entityNameLower}}Service.findById(id);
            }

            @PostMapping
            public {{entityName}} create(@RequestBody {{entityName}} {{entityNameLower}}) {
                return {{entityNameLower}}Service.save({{entityNameLower}});
            }

            @DeleteMapping("/{id}")
            public void delete(@PathVariable Long id) {
                {{entityNameLower}}Service.deleteById(id);
            }
        }
        ```

3. **Define Type Mappings**

   The `types.json` file will define the mappings for the data types used in the templates. For example:
    ```json
    {
        "String": "String",
        "Integer": "Integer",
        "Long": "Long",
        "Boolean": "Boolean",
        "Date": "LocalDate"
    }
    ```

4. **Using the Templates**

   When you run Auctor, it will use the templates and type definitions to generate the code. For example, given the following JDL input:
    ```jdl
    entity Person {
        name String required
        age Integer
    }

    relationship OneToMany {
        Person { friend } to Person
    }

    enum Gender {
        MALE, FEMALE, OTHER
    }
    ```

   Auctor will generate the corresponding Spring Boot code using the templates defined in `.auctor/spring`.

This approach will allow developers to tailor Auctor to their specific needs, supporting a wide range of languages and frameworks by simply defining the necessary templates and type mappings.

## Usage

1. Create an input file with the JDL specification, for example `entity.jdl`:
    ```jdl
    entity Person {
        name String required
        age Integer
    }

    relationship OneToMany {
        Person { friend } to Person
    }

    enum Gender {
        MALE, FEMALE, OTHER
    }
    ```

2. Run the project:
    ```sh
   ./build/bin/native/debugExecutable/auctor.kexe --jdl ./entity.jdl
    ```

3. The code generator will process the input file and generate the corresponding code.
